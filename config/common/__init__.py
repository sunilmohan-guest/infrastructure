# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import changes

from .. import conf

change_sources = [
    changes.GitPoller(conf.conf['repos']['freedom_maker']['url'],
                      workdir='freedom-maker-git-poller',
                      project='freedom-maker', branch='main',
                      pollInterval=300),
]


def rsync_common_opts():
    """Return common rsync options such as for non-standard SSH port."""
    port = conf.conf['upload']['port']
    if port in ('22', '', None):
        return []

    return [f'--rsh=ssh -p {port}']


def rsync_url(upload_path):
    """Return the rsync URL for a given upload path."""
    user = conf.conf['upload']['user']
    host = conf.conf['upload']['host']
    return f'{user}@{host}:{upload_path}'
