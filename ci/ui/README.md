# UI

This directory contains the user interface elements of the FreedomBox continuous integration system.

# Development

## Status Data
To simulate the UI as it appears on https://ci.freedombox.org/, status files for the build pipelines are required. They can be downloaded from the CI server by executing the script called `fetch_status_files.py`. The files must be in the directory ui/status.

## Server

You can start a server in Python to view your changes using the following command.
```
$ python3 -m http.server 8080
```

The web interface will now be available at http://localhost:8080/

You can then make changes to the UI and see them by refreshing the page.
